from flask import Flask
from flask_api import FlaskAPI, exceptions, status
import os
import requests
import json
import urllib2
app = Flask(__name__)



def internet_on():
	try:
		response = urllib2.urlopen("http://www.google.com")
		print "internet_on"
		return True
	except urllib2.URLError as err:
		pass
	return False

@app.route('/', methods=['GET'])
def hello():
	if not internet_on():
		res = {"Error":"Not Connected to Internet"}
		return json.dumps(res,sort_keys=True,indent=4)
	else:
		string = 'Hello Xor-Bots!' + '\nWelcome to Currency Exchange App\nSee the money Rise and Fall'
		return string




@app.route('/exchange_list',methods=['GET'])
def exchange_list():
	if not internet_on():
		res = {"Error":"Not Connected to Internet"}
		return json.dumps(res)
	else:
		url = ("http://www.apilayer.net/api/list?access_key=49902f370dbfba224fe3f991a6252952&format=1")
		response = requests.get(url)
		response_json =  response.json()   # need to convert 'Response' object to JSON object
		if response.status_code == 200:
			for k,v in response_json.iteritems():
				if k == 'currencies' or k == "error":
					result = json.dumps(v,sort_keys=True,indent=4)  #json.loads loads the json object into dict while json.dumps dumps dict into json object
					return result


@app.route('/exchange',methods=['GET'])
def exchange():
	if not internet_on():
		res = {"Error":"Not Connected to Internet"}
		return json.dumps(res)
	else:
		url = ("http://www.apilayer.net/api/live?access_key=49902f370dbfba224fe3f991a6252952&format=1")
		response = requests.get(url)
		response_json =  response.json()   # need to convert 'Response' object to JSON object
		if response.status_code == 200:
			for k,v in response_json.iteritems():
				if k == 'quotes' or k == "error":
					result = json.dumps(v,sort_keys=True,indent=4) #json.loads loads the json object into dict while json.dumps dumps dict into json object
					return result

@app.route('/exchange_countrycode/<country_code>',methods=['GET'])
def exchange_countrycode(country_code):
	if not internet_on():
		res = {"Error":"Not Connected to Internet"}
		return json.dumps(res)
	else:
		url = ("http://www.apilayer.net/api/live?access_key=49902f370dbfba224fe3f991a6252952&format=1&currencies=%s") % country_code
		response = requests.get(url)
		response_json =  response.json()   # need to convert 'Response' object to JSON object
		if response.status_code == 200:
			for k,v in response_json.iteritems():
				if k == 'quotes' or k == "error":
					result = json.dumps(v,sort_keys=True,indent=4) #json.loads loads the json object into dict while json.dumps dumps dict into json object
					return result
	



@app.route('/exchange(YYYY-MM-DD)/<date>',methods=['GET','POST'])
def exchange_date(date):
	if not internet_on():
		res = {"Error":"Not Connected to Internet"}
		return json.dumps(res)

	else:
		url = ("http://apilayer.net/api/historical?access_key=49902f370dbfba224fe3f991a6252952&format=1&date=%s") % date
		response = requests.get(url)
		response_json =  response.json()
		if response.status_code == 200:
			for k,v in response_json.iteritems():
				if k == 'quotes' or k == "error":
					result = json.dumps(v,sort_keys=True,indent=4) #json.loads loads the json object into dict while json.dumps dumps dict into json object
					return result


if __name__ == '__main__':
	port = int(os.environ.get('PORT', 5000))
	app.run(host='0.0.0.0', port=port, debug=True)
	